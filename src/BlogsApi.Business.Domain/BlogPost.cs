﻿using System;

namespace BlogsApi.Business.Domain
{
    public class BlogPost: EntityBase        
    {
        public string Topic { get; set; }

        public DateTime PublicationDate { get; set; }

        public string Author { get; set; }

        public string Text { get; set; }
    }
}
