﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;

namespace BlogsApi.DataAccess.EF
{
    using Business.Domain;
    using Contracts;
    using System.Data;
    public class SQLiteDbContext: DbContext, IDbContext
    {
        private DbTransaction _transaction;
        private ObjectContext _objectContext;

        public IDbSet<BlogPost> BlogPosts { get; set; }

        public new IDbSet<TEntity> Set<TEntity>()             
            where TEntity : EntityBase
        {
            return base.Set<TEntity>();
        }

        public void SetAsAdded<TEntity>(TEntity entity) 
            where TEntity : EntityBase
        {
            UpdateEntityState(entity, EntityState.Added);
        }

        public void SetAsModified<TEntity>(TEntity entity) 
            where TEntity : EntityBase
        {
            UpdateEntityState(entity, EntityState.Modified);
        }

        public void SetAsDeleted<TEntity>(TEntity entity) 
            where TEntity : EntityBase
        {
            UpdateEntityState(entity, EntityState.Deleted);
        }

        public void BeginTransaction()
        {
            this._objectContext = ((IObjectContextAdapter)this).ObjectContext;
            if (_objectContext.Connection.State == ConnectionState.Open)
            {
                return;
            }
            _objectContext.Connection.Open();
            _transaction = _objectContext.Connection.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                BeginTransaction();
                SaveChanges();
                _transaction.Commit();
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        public async Task CommitAsync()
        {
            try
            {
                BeginTransaction();
                await SaveChangesAsync();
                _transaction.Commit();
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        private void UpdateEntityState<TEntity>(TEntity entity, EntityState entityState) 
            where TEntity : EntityBase
        {
            var dbEntityEntry = GetDbEntityEntrySafely(entity);
            dbEntityEntry.State = entityState;
        }

        private DbEntityEntry GetDbEntityEntrySafely<TEntity>(TEntity entity) 
            where TEntity : EntityBase
        {
            var dbEntityEntry = Entry<TEntity>(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                Set<TEntity>().Attach(entity);
            }
            return dbEntityEntry;
        }

    }
}
