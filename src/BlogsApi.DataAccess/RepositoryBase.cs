﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlogsApi.DataAccess
{
    using Contracts;
    using Business.Domain;

    public class RepositoryBase<TEntity>: IRepository<TEntity> 
        where TEntity: EntityBase        
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDbSet<TEntity> _dbEntitySet;
        private bool _disposed;

        public RepositoryBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;        
            _dbEntitySet = _unitOfWork.Context.Set<TEntity>();
        }

        public IUnitOfWork UnitOfWork { get { return _unitOfWork;  } }

        public TEntity Retrieve(int id)
        {
            return _dbEntitySet.Where(x => x.Id == id).FirstOrDefault();
        }

        public Task<TEntity> RetrieveAsync(int id)
        {
            return _dbEntitySet.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public IQueryable<TEntity> RetrieveMultiple()
        {
            return _dbEntitySet.AsQueryable();
        }

        public Task<IQueryable<TEntity>> RetrieveMultipleAsync()
        {
            return Task.FromResult(_dbEntitySet.AsQueryable());
        }

        public IQueryable<TEntity> RetrieveMultiple(Expression<Func<TEntity, bool>> expression)
        {
            return _dbEntitySet.Where(expression).AsQueryable();
        }

        public Task<IQueryable<TEntity>> RetrieveMultipleAsync(Expression<Func<TEntity, bool>> expression)
        {
            return Task.FromResult(_dbEntitySet.Where(expression).AsQueryable());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _unitOfWork.Dispose();
            }
            _disposed = true;
        }
    }
}
