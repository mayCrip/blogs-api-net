﻿using System;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Collections;

namespace BlogsApi.DataAccess
{
    using Contracts;
    using Business.Domain;
    
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;

        public UnitOfWork(IDbContext dbContext)
        {
            Context = dbContext;
        }

        public IDbContext Context { get; private set; }

        public void BeginTransaction()
        {
            Context.BeginTransaction();
        }

        public void Commit()
        {
            Context.Commit();
        }

        public Task CommitAsync()
        {
            return Context.CommitAsync();
        }

        public void Rollback()
        {
            Context.Rollback();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                Context.Dispose();
            }
            _disposed = true;
        }
    }
}
