﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BlogsApi.DataAccess.Contracts
{
    using Business.Domain;

    public interface IDbContext: IDisposable        
    {
        IDbSet<TEntity> Set<TEntity>()
            where TEntity : EntityBase;

        void SetAsAdded<TEntity>(TEntity entity)
            where TEntity : EntityBase;

        void SetAsModified<TEntity>(TEntity entity)
            where TEntity : EntityBase;

        void SetAsDeleted<TEntity>(TEntity entity)
            where TEntity : EntityBase;

        void BeginTransaction();

        void Commit();

        Task CommitAsync();

        void Rollback();
    }
}
