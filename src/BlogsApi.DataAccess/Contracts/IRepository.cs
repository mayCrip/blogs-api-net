﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlogsApi.DataAccess.Contracts
{
    using Business.Domain;

    public interface IRepository<TEntity>: IDisposable
        where TEntity: EntityBase
    {
        IUnitOfWork UnitOfWork { get; }

        TEntity Retrieve(int id);

        Task<TEntity> RetrieveAsync(int id);

        IQueryable<TEntity> RetrieveMultiple();

        Task<IQueryable<TEntity>> RetrieveMultipleAsync();

        IQueryable<TEntity> RetrieveMultiple(Expression<Func<TEntity, bool>> expression);

        Task<IQueryable<TEntity>> RetrieveMultipleAsync(Expression<Func<TEntity, bool>> expression);
    }
}
