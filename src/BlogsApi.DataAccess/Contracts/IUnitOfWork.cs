﻿using System;
using System.Threading.Tasks;

namespace BlogsApi.DataAccess.Contracts
{
    using Business.Domain;

    public interface IUnitOfWork: IDisposable
    {
        void Commit();

        Task CommitAsync();

        void BeginTransaction();

        void Rollback();

        IDbContext Context { get; }
    }
}
