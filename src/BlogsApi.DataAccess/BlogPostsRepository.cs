﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlogsApi.DataAccess
{
    using Business.Domain;
    using Contracts;
    public class BlogPostsRepository : RepositoryBase<BlogPost>, IBlogPostRepository        
    {
        public BlogPostsRepository(IUnitOfWork unitOfWork)
            :base(unitOfWork)
        {            
        }
    }
}
