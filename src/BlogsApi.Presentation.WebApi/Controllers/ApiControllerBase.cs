﻿using System.Net.Http;
using System.Web.Http;

namespace BlogsApi.Presentation.WebApi.Controllers
{
    public class ApiControllerBase : ApiController
    {
        protected T GetService<T>()
        {
            
            return (T)this.Request.GetDependencyScope().GetService(typeof(T));
        }
    }
}
