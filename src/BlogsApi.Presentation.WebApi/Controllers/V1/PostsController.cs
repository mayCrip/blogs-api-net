﻿using System.Web.Http;
using System.Threading.Tasks;

namespace BlogsApi.Presentation.WebApi.Controllers.V1
{
    using Models;
    using Business.Services.Contracts;
    using Business.Services;
    using DataAccess;
    using DataAccess.EF;
    using Facade;
    [RoutePrefix(Const.Routes.V1Prefix)]
    public class PostsController : ApiControllerBase
    {
        private const int _itemsPerPage = 10;

        [Route(Const.Routes.Posts)]
        [HttpGet]
        public async Task<RetrieveMultipleResponse> Get()
        {
            var svcFacade = base.GetService<PostsFacade>();
            var posts = await svcFacade.BlogPosts.RetrieveMultipleAsync(1, _itemsPerPage);

            return new RetrieveMultipleResponse()
            {
                Posts = posts
            };
        }

        [Route(Const.Routes.Posts)]
        [HttpGet]
        public async Task<RetrieveMultipleResponse> Get([FromUri]RetrieveMultipleRequest request)
        {
            var svcFacade = base.GetService<PostsFacade>();
            var posts = await svcFacade.BlogPosts.RetrieveMultipleAsync(request.PageNumber, _itemsPerPage);

            return new RetrieveMultipleResponse()
            {
                Posts = posts
            };
        }

        [Route(Const.Routes.GetPost)]
        public async Task<RetrieveResponse> Get(int id)
        {
            var svcFacade = base.GetService<PostsFacade>();
            var post = await svcFacade.BlogPosts.RetrieveAsync(id);

            return new RetrieveResponse()
            {
                Post = post
            };
        }
    }
}