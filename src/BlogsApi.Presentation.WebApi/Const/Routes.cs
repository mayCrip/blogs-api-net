﻿namespace BlogsApi.Presentation.WebApi.Const
{
    public static class Routes
    {
        public const string V1Prefix = "api/v1";

        public const string Posts = "posts";

        public const string GetPost = "posts/{id:int}";
    }
}