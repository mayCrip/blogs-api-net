﻿using System.Collections.Generic;

namespace BlogsApi.Presentation.WebApi.Models
{
    using Business.Domain;
    using System;
    public class RetrieveMultipleResponse        
    {
        public IEnumerable<BlogPost> Posts { get; set; }
    }
}