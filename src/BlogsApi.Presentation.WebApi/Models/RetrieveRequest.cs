﻿namespace BlogsApi.Presentation.WebApi.Models
{
    using Business.Domain;
        
    public class RetrieveResponse        
    {
        public BlogPost Post { get; set; }
    }
}