﻿namespace BlogsApi.Presentation.WebApi.Models
{
    public class RetrieveMultipleRequest
    {
        public int PageNumber { get; set; }
    }
}