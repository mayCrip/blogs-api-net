﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogsApi.Presentation.WebApi.Facade
{
    using Business.Services.Contracts;

    public class PostsFacade
    {
        private readonly IBlogPostsService _service;

        public PostsFacade(IBlogPostsService service)
        {
            _service = service;
        }

        public IBlogPostsService BlogPosts { get { return _service; } }
    }
}