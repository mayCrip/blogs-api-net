﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace BlogsApi.Business.Services
{
    using Domain;
    using Contracts;
    using DataAccess.Contracts;

    public class BlogPostsService : IBlogPostsService, IDisposable
    {
        private IBlogPostRepository _blogPostsRepository;
        private bool _disposed = false;

        public BlogPostsService(IBlogPostRepository reposistory)
        {
            _blogPostsRepository = reposistory;
        }

        public BlogPost Retrieve(int id)
        {
            return _blogPostsRepository.Retrieve(id);
        }

        public Task<BlogPost> RetrieveAsync(int id)
        {
            return _blogPostsRepository.RetrieveAsync(id);
        }

        public IEnumerable<BlogPost> RetrieveMultiple(int pageNumber, int itemsPerPage)
        {
            return _blogPostsRepository.RetrieveMultiple().Skip((pageNumber - 1) * itemsPerPage).Take(itemsPerPage);
        }

        public async Task<IEnumerable<BlogPost>> RetrieveMultipleAsync(int pageNumber, int itemsPerPage)
        {
            var posts = await _blogPostsRepository.RetrieveMultipleAsync();            
            return posts.Skip((pageNumber - 1) * itemsPerPage).Take(itemsPerPage);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
               _blogPostsRepository.Dispose();
            }
            _disposed = true;
        }
    }
}
