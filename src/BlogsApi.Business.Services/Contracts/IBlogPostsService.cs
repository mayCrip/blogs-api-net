﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogsApi.Business.Services.Contracts
{
    using Domain;

    public interface IBlogPostsService        
    {
        BlogPost Retrieve(int id);

        Task<BlogPost> RetrieveAsync(int id);

        IEnumerable<BlogPost> RetrieveMultiple(int pageNumber, int itemsPerPage);

        Task<IEnumerable<BlogPost>> RetrieveMultipleAsync(int pageNumber, int itemsPerPage);
    }
}
